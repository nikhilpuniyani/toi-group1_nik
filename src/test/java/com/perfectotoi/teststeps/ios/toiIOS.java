package com.perfectotoi.teststeps.ios;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;

import com.perfectotoi.pages.HomeTestPageIOS;
import com.perfectotoi.util.utility;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class toiIOS extends WebDriverBaseTestPage<WebDriverTestPage>
		implements
			com.perfectotoi.CommonStep.CommonStep {
	HomeTestPageIOS homePage = new HomeTestPageIOS();

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {

	}

	@QAFTestStep(description = "user opens navigation bar")
	public void userOpensNavigationBar() {
		do {
			homePage.getNaivgationbarHomepageIos().click();
		} while (!homePage.getNaivgationbarHomepageIos().isPresent());
		Reporter.log("Navigation bar is opened");
	}

	@QAFTestStep(description = "user get all tab list")
	public void userGetAllTabList() {
		List<QAFWebElement> list = homePage.getListTabHomepageIos();
		for (int i = 0; i < list.size(); i++) {
			try {
				if (list.get(i).isDisplayed()) {
					Reporter.log("Tab name is " + list.get(i).getAttribute("name"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	//Verification of Saved Story
	 @QAFTestStep(description = "verify saved story")
	 public void verifySavedStory() {
		homePage.getSaved_story_text().isPresent();
		Reporter.logWithScreenShot("Successfully Verified Saved Story",
				MessageTypes.Pass);
	}

	@QAFTestStep(description = "user navigates to {0} page")
	public void userNavigatesToPage(String data) {
		homePage.getBtnSettingIos().click();
	}

	@QAFTestStep(description = "user selects the {0} city")
	public String userSelectsTheCity(String data) {

		homePage.getTextCityHomepageIos().click();
		CommonStep.sendKeys(data, "citysearchfield.homepage.ios");
		homePage.getRadioButtonSearchDataIos().waitForVisible();
		homePage.getRadioButtonSearchDataIos().click();
		homePage.getTextCityHomepageIos().waitForVisible();
		return data;

	}

	@QAFTestStep(description = "user navigates to home page")
	public void userNavigatesToHomePage() {
		homePage.getBtnBackNavigationIos().click();
		Reporter.logWithScreenShot("user navigated to homepage", MessageTypes.Pass);
	}

	@QAFTestStep(description = "user verifies the {0} tab")
	public void userVerifiesTheTab(String data) {
		String str = data.toUpperCase();
		QAFExtendedWebElement cityTab =
				new QAFExtendedWebElement("tab.city.homepage.ios");
		String cityText = cityTab.getText();
		Validator.verifyThat("the city name tab in homepage is verified ",
				cityText.contains(str), Matchers.is(true));

	}

	/**
	 * navigate to respective page
	 */

	/**
	 * Clicking on Sports tab.
	 */
	@QAFTestStep(description = "user navigate to {0}")
	public void userNavigateToPage(String page) {
		List<QAFWebElement> list = homePage.getListTabHomepageIos();
		if (page.equalsIgnoreCase("India page")) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getAttribute("name").equalsIgnoreCase("India")) {
					list.get(i).click();
					break;
				}
			}
		}
		//clicking on Saved Stories
		else if(page.equalsIgnoreCase("Saved Stories")) {
			homePage.getMenuIcon().click();
			homePage.getSaved_stories_icon().click();
			Reporter.logWithScreenShot("Successfully Clicked on Saved Stories",
					MessageTypes.Pass);
		}

	}

	/**
	 * verify India page header
	 */
	@QAFTestStep(description = "user verify India page header")
	public void userVerifySportsPageHeader() {

		Validator.verifyThat("validating India page header",
				homePage.getLabelSportsSportPage().isDisplayed(), Matchers.equalTo(true));
	}
	
	//clicks on the top story
	@QAFTestStep(description = "User Open story")
	public void userOpenStory() {
		homePage.getStoryText().click();
		Reporter.logWithScreenShot("Successfully Clicked on Story", MessageTypes.Pass);
	}

	//Click on Button
	@QAFTestStep(description = "user clicks on {0}")
	public void userClicksOn(String btn) {
		// CommonStep.waitForPresent("contentdesc.all");

		if (btn.equalsIgnoreCase("star")) {
			homePage.getBookmark().click();
			Reporter.logWithScreenShot("Successfully Clicked on Start Icon",
					MessageTypes.Pass);
			homePage.getBackButton().click();
		} else {
			CommonStep.click(String.format(
					ConfigurationManager.getBundle().getString("contentdesc.all"), btn));

		}
	}

	@Override
	public QAFExtendedWebElement creatElement(String loc, String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void Small(String btn) {
		// TODO Auto-generated method stub

	}

	@Override
	public void userSearchFor(String loc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void userClicksOnButtonAndSendCharacter(String btn, String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void userLoginToTheApplication() {
		// TODO Auto-generated method stub

	}

	@Override
	public void userVerifiesTheProfilePage() {
		// TODO Auto-generated method stub

	}

	@Override
	public void userLogoutTheApplication(String data) {
		// TODO Auto-generated method stub

	}
	@QAFTestStep(description = "user close the app")
	public void userCloseTheApp() throws InterruptedException {
		utility.closeApp("TOI");
	}
	@QAFTestStep(description = "user scroll down till business")
	public void userScrollDownTillBusiness() throws InterruptedException {
		Reporter.log("scroll up to Business section");

		do {
			Map<String, Object> params = new HashMap<>();
			params.put("start", "800,800");
			params.put("end", "1,1");
			// params.put("duration", "3");
			Object res = driver.executeScript("mobile:touch:swipe", params);
			Reporter.log("scrolling now");
		}while (! homePage.getSensexsectionhomepage().isPresent());
		Reporter.log("got the sensex"
				+ homePage.getValuesectionhomepage().getAttribute("name"));

		if (homePage.getValuesectionhomepage().getAttribute("name").contains("-")) {
			Reporter.log("Todays sensex is decreased by "
					+ homePage.getValuesectionhomepage().getAttribute("name"));
		} else {
			Reporter.log("Todays sensex is increased by "
					+ homePage.getValuesectionhomepage().getAttribute("name"));
		}

	}

}
