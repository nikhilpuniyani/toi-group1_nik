package com.perfectotoi.pages;

import java.util.List;

import com.perfectotoi.CommonStep.HomePageCommon;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomeTestPageIOS extends WebDriverBaseTestPage<WebDriverTestPage> implements HomePageCommon{

	@FindBy(locator = "naivgationbar.homepage.ios")
	private QAFWebElement naivgationbarHomepageIos;
	@FindBy(locator = "list.tab.homepage.ios")
	private List<QAFWebElement> listTabHomepageIos;
	@FindBy(locator = "contentdesc.all.ios")
	private QAFWebElement contentdescAllIos;
	@FindBy(locator = "story.text")
	private QAFWebElement storyText;
	@FindBy(locator = "bookmark.icon")
	private QAFWebElement bookmark;
	@FindBy(locator = "menu.icon")
	private QAFWebElement menuIcon;
	@FindBy(locator = "saved_stories_icon")
	private QAFWebElement saved_stories_icon;
	@FindBy(locator = "back.button")
	private QAFWebElement backButton;
	@FindBy(locator = "saved_story.text")
	private QAFWebElement saved_story_text;
	@FindBy(locator = "btn.settings.homepage.ios")
	private QAFWebElement btnSettingIos;
	@FindBy(locator = "text.city.homepage.ios")
	private QAFWebElement textCityHomepageIos;
	@FindBy(locator = "radiobutton.searchdata.ios")
	private QAFWebElement radioButtonSearchDataIos;
	@FindBy(locator = "btn.back.navigation.ios")
	private QAFWebElement btnBackNavigationIos;
	@FindBy(locator = "label.sports.sportPage")
	private QAFWebElement labelSportsSportPage;
	
	@FindBy(locator = "sensex.section.homepage")
	private QAFWebElement sensexsectionhomepage;
	
	@FindBy(locator = "value.section.homepage")
	private QAFWebElement valuesectionhomepage;
	
	
	
	public QAFWebElement getValuesectionhomepage() {
		return valuesectionhomepage;
	}

	public QAFWebElement getSensexsectionhomepage() {
		return sensexsectionhomepage;
	}

	public QAFWebElement getStoryText() {
		return storyText;
	}

	public QAFWebElement getBookmark() {
		return bookmark;
	}

	public QAFWebElement getMenuIcon() {
		return menuIcon;
	}

	public QAFWebElement getBackButton() {
		return backButton;
	}

	
	
	public QAFWebElement getSaved_stories_icon() {
		return saved_stories_icon;
	}

	public QAFWebElement getSaved_story_text() {
		return saved_story_text;
	}

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getNaivgationbarHomepageIos() {
		return naivgationbarHomepageIos;
	}

	public List<QAFWebElement> getListTabHomepageIos() {
		return listTabHomepageIos;
	}

	public QAFWebElement getContentdescAllIos() {
		return contentdescAllIos;
	}
	public QAFWebElement getTextCityHomepageIos() {
		return textCityHomepageIos;
	}
	public QAFWebElement getRadioButtonSearchDataIos() {
		return radioButtonSearchDataIos;
	}
		public QAFWebElement getBtnBackNavigationIos() {
			return btnBackNavigationIos;
	}
		public QAFWebElement getBtnSettingIos() {
			return btnSettingIos;
	}

	public QAFWebElement getLabelSportsSportPage() {
		return labelSportsSportPage;
	}
}
