package com.perfectotoi.CommonStep;

import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public interface CommonStep {
	public void userScrollDownTillBusiness() throws InterruptedException; 
	public void userOpensNavigationBar();
	public void userGetAllTabList();
	public QAFExtendedWebElement creatElement(String loc, String key);
	public void userClicksOn(String btn);
	public void Small(String btn);
	public void userSearchFor(String loc);
	public void userClicksOnButtonAndSendCharacter(String btn, String text);
	public void verifySavedStory();
	public void userLoginToTheApplication();
	public void userVerifiesTheProfilePage();
	public void userLogoutTheApplication(String data);
	public void userNavigatesToPage(String data);
	public String userSelectsTheCity(String data);
	public void userNavigatesToHomePage();
	public void userVerifiesTheTab(String data);
	public void userNavigateToPage(String page);
	public void userVerifySportsPageHeader();
	public void userOpenStory();
	public void  userCloseTheApp() throws InterruptedException;
	
	
}
